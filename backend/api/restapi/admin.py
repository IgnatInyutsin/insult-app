from django.contrib import admin
from restapi.app.models import *

admin.site.register(HospitalProfile)
admin.site.register(DoctorProfile)
admin.site.register(PacientProfile)