import uuid
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from restapi.app.models import *
from .serializers import *
from rest_framework.serializers import ValidationError
from restapi.app.permissions import GetAnyProfiles
from .permissions import GetHospitalProfile
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.db import transaction
import uuid
from rest_framework.authentication import TokenAuthentication

class HospitalViewSet(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):
    pagination_class = PageNumberPagination
    queryset = HospitalProfile.objects.all().order_by('id')
    serializer_class = HospitalSerializer
    permission_classes = (AllowAny,)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return HospitalPostSerializer
        else:
            return HospitalSerializer

    def get_permissions(self):
        if self.request.method == "POST":
            return (IsAuthenticated(), GetAnyProfiles(),)
        else:
            return (AllowAny(),)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        # Получаем наш сериализатор
        serializer = self.get_serializer(data=request.data)

        # Валидация
        if not serializer.is_valid(raise_exception=True):
            raise ValidationError(serializer.errors)

        user = Token.objects.get(key=request.headers.get("Authorization").replace("Token ", "")).user

        hospital = HospitalProfile(name=self.request.data.get("name"),
                                   doctors_count=0,
                                   user=user)
        hospital.save()

        return Response(status=201)

# Вьюха для активации доктор аккаунта
class DoctorActivationViewSet(mixins.CreateModelMixin,
                              mixins.ListModelMixin,
                              viewsets.GenericViewSet):
    queryset = DoctorProfile.objects.all().order_by('id')
    serializer_class = DoctorActivationSerializer
    permission_classes = (IsAuthenticated, GetHospitalProfile,)

    def get_queryset(self):
        if this.request.method == "POST":
            user = Token.objects.get(key=request.headers.get("Authorization").replace("Token ", "")).user
            return DoctorProfile.objects.filter(user=user).order_by('id')

        return self.queryset

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        # Получаем наш сериализатор
        serializer = self.get_serializer(data=request.data)

        # Валидация
        if not serializer.is_valid(raise_exception=True):
            raise ValidationError(serializer.errors)

        # получаем пользователя
        user = Token.objects.get(key=request.headers.get("Authorization").replace("Token ", "")).user
        # Получаем госпиталь
        hospital = HospitalProfile.objects.get(user=user)

        my_doctor = DoctorProfile.objects.get(id=request.data.get("id"))
        my_doctor.activated = True
        my_doctor.hospital = hospital
        my_doctor.save()

        hospital.doctors_count += 1
        hospital.save()

        return Response(status=201)