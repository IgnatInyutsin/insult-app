from restapi.app.models import *
from rest_framework import serializers
from restapi.app.serializers import UserForGetSerializer

# Сериализатор для отдельной больницы
class HospitalSerializer(serializers.HyperlinkedModelSerializer):
    owner = UserForGetSerializer()
    class Meta:
        model = HospitalProfile
        fields = ["id",
                  "name",
                  "doctors_count",
                  "owner"]

# Сериализатор для отдельной больницы
class DoctorActivationSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.UUIDField()
    class Meta:
        model = DoctorProfile
        fields = ["id"]

    def validate_id(self, field):
        doctor = DoctorProfile.objects.filter(id=int(field))

        if len(doctor) == 0:
            raise serializers.ValidationError("Uncorrect Doctor ID")

        doctor = doctor[0]

        if doctor.hospital != None:
            raise serializers.ValidationError("Doctor already have hospital")

        return field