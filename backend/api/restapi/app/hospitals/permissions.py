from rest_framework import permissions
from rest_framework.authtoken.models import Token
from restapi.app.models import *

class GetHospitalProfile(permissions.BasePermission):
    def has_permission(self, request, view):
        # собираем залогиненного пользователя
        user = Token.objects.filter(key=request.headers.get("Authorization", "").replace("Token ", ""))

        if len(user) < 1:
            return False

        user = user[0].user

        # проверяем на наличие аккаунта госпитали
        return len(HospitalProfile.objects.filter(user=user)) == 1