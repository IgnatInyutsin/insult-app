from restapi.app.models import *
from rest_framework import serializers
from django.contrib.auth import get_user_model

# Сериализатор для пользователя
class UserForGetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name", "last_login", "date_joined"]