from django.db import models
from django.contrib.auth.models import User
import uuid

# модель с симптомами
class Symptom(models.Model):
    name = models.TextField()

# модель с рекомендациями по лечению
class Recommendation(models.Model):
    name = models.TextField()
    description = models.TextField()

# Модель общего профиля пользователя для расширения стандартной модели DJANGO
class HospitalProfile(models.Model):
    # ID профиля
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Объект пользователя
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="owner")
    # Название учреждения
    name = models.TextField(unique=True)
    # количество привязанных аккаунтов
    doctors_count = models.IntegerField(default=0)
    # активность
    activated = models.BooleanField(default=True)

    def __str__(self):
        return "Hospital " + self.name

# Модель общего профиля пользователя для расширения стандартной модели DJANGO
class DoctorProfile(models.Model):
    # ID профиля
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Объект пользователя
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user")
    # к кому привязан
    hospital = models.OneToOneField(HospitalProfile, on_delete=models.CASCADE, null=True, blank=True, related_name="hospital", default=None)
    # количество пациентов
    case_count = models.IntegerField(default=0)
    # активность
    activated = models.BooleanField(default=False)

    def __str__(self):
        return "Doctor " + self.user.first_name + " " + self.user.last_name

class PacientProfile(models.Model):
    # ID профиля
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # Объект пользователя
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="pacient_user")

    def __str__(self):
        return "Pacient " + self.name

class Case(models.Model):
    # ID профиля
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # пациент
    pacient = models.ForeignKey(PacientProfile, on_delete=models.CASCADE)
    # фотография его мрт головного мозга
    scan = models.FileField(null=True, blank=True)
    # посторонние симптомы
    symptoms = models.ManyToManyField(Symptom, related_name="symptoms")
    # статус обработки фотографии
    is_processed = models.BooleanField(default=False)
    # рекомендации по лечению
    recommendations = models.ManyToManyField(Symptom, null=True, blank=True, related_name="recommendations")
