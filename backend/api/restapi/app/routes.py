from django.urls import include, path
from rest_framework import routers
from restapi.app.hospitals.views import *
from restapi.app.doctors.views import *
from restapi.app.pacients.views import *

#устанавливаем пути
router = routers.DefaultRouter()
router.register('pacients', PacientViewSet)
router.register('hospitals/me/doctors', DoctorActivationViewSet)
router.register('hospitals', HospitalViewSet)
router.register('doctors', DoctorViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls))
]
