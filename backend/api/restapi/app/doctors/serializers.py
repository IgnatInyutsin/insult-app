from restapi.app.models import *
from rest_framework import serializers
from restapi.app.serializers import UserForGetSerializer

# Сериализатор для отдельной больницы
class SmallHospitalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = HospitalProfile
        fields = ["id",
                  "name"]

# Сериализатор для доктора
class DoctorSerializer(serializers.HyperlinkedModelSerializer):
    user = UserForGetSerializer()
    hospital = SmallHospitalSerializer()
    class Meta:
        model = DoctorProfile
        fields = ["id", "user", "hospital", "case_count", "activated"]

# Сериализатор для отдельного доктора
class DoctorPostSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DoctorProfile
        fields = []