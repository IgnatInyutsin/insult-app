import uuid
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from restapi.app.models import *
from .serializers import *
from rest_framework.serializers import ValidationError
from restapi.app.permissions import GetAnyProfiles
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.db import transaction
import uuid

class DoctorViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    pagination_class = PageNumberPagination
    queryset = DoctorProfile.objects.all().order_by('id')
    serializer_class = DoctorSerializer
    permission_classes = (AllowAny,)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return DoctorPostSerializer

        return DoctorSerializer

    def get_permissions(self):
        if self.request.method == "POST":
            return (IsAuthenticated(), GetAnyProfiles(),)
        else:
            return (AllowAny(),)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        user = Token.objects.get(key=request.headers.get("Authorization").replace("Token ", "")).user
        doctor = DoctorProfile(user=user)
        doctor.save()

        return Response(status=201)