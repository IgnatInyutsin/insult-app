import uuid
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny, IsAuthenticated
from restapi.app.models import *
from .serializers import *
from rest_framework.serializers import ValidationError
from restapi.app.permissions import GetAnyProfiles
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.db import transaction
import uuid
from rest_framework.authentication import TokenAuthentication

class PacientViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):
    pagination_class = PageNumberPagination
    queryset = PacientProfile.objects.all().order_by('id')
    serializer_class = PacientSerializer
    permission_classes = (AllowAny,)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return EmptyPacientSerializer
        else:
            return PacientSerializer

    def get_permissions(self):
        if self.request.method == "POST":
            return (IsAuthenticated(), GetAnyProfiles(),)
        else:
            return (AllowAny(),)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        user = Token.objects.get(key=request.headers.get("Authorization").replace("Token ", "")).user

        pacient = PacientProfile(user=user)
        pacient.save()

        return Response(status=201)