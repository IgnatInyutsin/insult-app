from restapi.app.models import *
from restapi.app.serializers import UserForGetSerializer
from rest_framework import serializers

# Сериализатор для пациентов
class PacientSerializer(serializers.HyperlinkedModelSerializer):
    pacient_user = UserForGetSerializer()

    class Meta:
        model = PacientProfile
        fields = ["id", "pacient_user"]

class EmptyPacientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = []